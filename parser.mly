

/* Analyseur syntaxique pour Petit Go */

%{
  open Ast
  exception Parsing_Error

  let rec l_expr_id l = match l with
    | ((Evar s), posit) :: q -> s :: (l_expr_id q)
    | [] -> []
    | _ -> exit 1  (* Si l'on arrive ici, il y a eu une erreure de syntaxe*)

%}

%token PLUS PLUSP
%token MINUS MINUSM
%token TIMES 
%token OR 
%token AND AAND 
%token NEQ 
%token LESS 
%token LEQ 
%token GREATER 
%token GEQ  
%token DIV 
%token MOD 
%token LPAREN 
%token RPAREN 
%token BEGIN 
%token END 
%token EXCL
%token FMT
%token COMMA 
%token DOT EEQUAL
%token SEMICOLON COLON EQUAL
%token <nativeint> CST
%token <string> IDENT
%token <string> STR
%token IF ELSE FALSE FOR FUNC MAIN IMPORT NIL PACKAGE RETURN STRUCT TRUE TYPE VAR 
%token EOF

/* Les priorités et associativités des tokens */

%left OR
%left AND

%left  EEQUAL NEQ GREATER GEQ LESS LEQ

%left MINUS PLUS
%left TIMES DIV MOD

%left DOT





/* Point d'entrée de la grammaire */
%start fichier

/* Type des valeurs renvoyées par l'analyseur syntaxique */
%type <Ast.fichier> fichier

%%

%inline pos(X): (* Ajoute les positions debut et fin de la regle de grammaire *)
    | x = X {x, ($startpos) }


separated_nonempty_list2(sep, X):
    | x = X; sep? {[x]}
    | x = X; sep; xs = separated_nonempty_list2(sep, X) {x :: xs}

separated_list2(sep, X):
    | sep? {[]}
    | x = X; sep; xs = separated_list2(sep, X) {x :: xs}



fichier:
  | PACKAGE MAIN SEMICOLON IMPORT FMT SEMICOLON blocs = decl* EOF
  { { import_fmt = true ;
      decls = blocs}}
  |PACKAGE MAIN SEMICOLON blocs = decl* EOF
  { { import_fmt = false ;
      decls = blocs}}

decl:
| s = pos(strct) { DecS(s) }
| f = pos(fct)   { Declf(f) }


strct:
|TYPE id = ident STRUCT BEGIN v = separated_nonempty_list2(SEMICOLON, pos(vars)) END SEMICOLON {Struct(id, v)}
|TYPE id = ident STRUCT BEGIN END SEMICOLON {Struct(id, [])}


fct:
| FUNC id = ident LPAREN RPAREN b = pos(bloc) SEMICOLON { Funcr(id, [], [], b)}
| FUNC id = ident LPAREN RPAREN tr = pos(type_ret) b = pos(bloc) SEMICOLON { Funcr(id, [], [tr], b)}
| FUNC id = ident LPAREN v = separated_nonempty_list2(COMMA, pos(vars)) RPAREN tr = pos(type_ret) b = pos(bloc) SEMICOLON { Funcr(id, v, [tr], b)}
| FUNC id = ident LPAREN v = separated_nonempty_list2(COMMA, pos(vars)) RPAREN b = pos(bloc) SEMICOLON { Funcr(id, v, [], b)}

vars:
  l = separated_nonempty_list(COMMA, ident); t = typee {Var(l, t)}

type_ret:
| t = typee { TR([t]) }
| LPAREN t = separated_nonempty_list2(COMMA, typee) RPAREN { TR(t) }

typee:
| id = ident { TIdent(id) }
| TIMES a =typee { TStar(a) }

expr :
| i = CST { Econst(i) }
| s = STR { Estring(s) }
| TRUE    { Ebool(true) }
| FALSE   { Ebool(false) }
| NIL     { Enil }
| LPAREN e = expr RPAREN { e }
| id = ident { Evar(id) }
| e = pos(expr) DOT id = ident { Edot( e, id) }
| id = ident LPAREN l = separated_list(COMMA, pos(expr))  RPAREN { Eapp(id, l) }
| FMT DOT id = ident LPAREN l = separated_list(COMMA, pos(expr)) RPAREN { if id = "Print" then Eprint(l) else raise Parsing_Error}
| EXCL e = pos(expr) { Eunop(ExclaP, e) }
| MINUS e = pos(expr) { Eunop(Minus, e)}
| AND e = pos(expr) { Eunop(UAnd, e)}
| TIMES e = pos(expr) {Eunop(Star, e) }
| e1 = pos(expr) b = binop e2 = pos(expr) { Ebinop(b, e1,e2) }

binop:
| EEQUAL { Eq }
| NEQ    { NEq }
| LESS   { L }
| LEQ    { LEq }
| GREATER { G }
| GEQ     {GEq }
| DIV     { Div }
| MOD     { Mod }
| PLUS    { Add }
| MINUS   { Sub }
| TIMES   { Mul }
| AAND { And }
| OR OR   { Or }

bloc:
|BEGIN SEMICOLON* END { Bloc([]) }
|BEGIN l= separated_nonempty_list2(SEMICOLON, instr) END { Bloc(l) }


instr:
| i= pos(instr_s) { IS(i) }
| b = pos(bloc)   { IBloc(b) }
| i = pos(instr_if) { Instif(i) }
| VAR li = separated_nonempty_list(COMMA, ident) t = pos(typee) EQUAL le =separated_nonempty_list(COMMA, pos(expr))
             { IVar(li, [t], le, ($startpos))}
| VAR li = separated_nonempty_list(COMMA, ident) t = pos(typee) 
             { IVar(li, [t], [], ($startpos))}
| VAR li = separated_nonempty_list(COMMA, ident) EQUAL le =separated_nonempty_list(COMMA, pos(expr))
             { IVar(li, [], le, ($startpos))}
| VAR li = separated_nonempty_list(COMMA, ident) 
             { IVar(li, [], [], ($startpos))}
| RETURN l = separated_list(COMMA, pos(expr)) { IReturn(l)}
| FOR b = pos(bloc)  { Ifor((Ebool(true), ($startpos)), b)}
| FOR e = pos(expr) b = pos(bloc) { Ifor(e, b)}
| FOR is = pos(instr_s) SEMICOLON e = pos(expr) SEMICOLON iss = pos(instr_s) b = pos(bloc) { IBloc(Bloc([IS(is) ; Ifor(e, (Bloc([IBloc(b) ; IS(iss)]), ($startpos)))]), ($startpos)) }
| FOR is = pos(instr_s) SEMICOLON e = pos(expr) SEMICOLON b = pos(bloc) { IBloc(Bloc([IS(is) ; Ifor(e, b)]), ($startpos)) }
| FOR SEMICOLON e = pos(expr) SEMICOLON iss = pos(instr_s) b = pos(bloc) { Ifor(e, (Bloc([IBloc(b) ; IS(iss)]) , ($startpos))) }
| FOR SEMICOLON e = pos(expr) SEMICOLON b = pos(bloc) { Ifor(e, b) }

instr_s:
| e = pos(expr) { Iexpr(e) }
| e = pos(expr) MINUSM { IMinus(e) }
| e = pos(expr) PLUSP { IPlus(e) }
| l1 = separated_nonempty_list(COMMA, pos(expr)) EQUAL l2 = separated_nonempty_list(COMMA, pos(expr))
      { IEq(l1,l2) } 
| l1 = separated_nonempty_list(COMMA, pos(expr)) COLON EQUAL l2 = separated_nonempty_list(COMMA, pos(expr))
      { IAs (l_expr_id l1,l2) }

instr_if:
| IF  e = pos(expr) b = pos(bloc) ELSE bb = pos(bloc) { Iifelse(e, b, bb)}
| IF  e = pos(expr) b = pos(bloc) ELSE bb = pos(instr_if) { Iifelif(e, b, bb)}
| IF  e = pos(expr) b = pos(bloc) { IIf(e, b)}

ident:
| id = IDENT { id }
| FMT        { "fmt" } 
