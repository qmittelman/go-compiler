(* Analyseur lexical pour mini-Turtle *)

{
  open Lexing
  open Parser

  exception Lexing_error of string

  (* tables des mots-clés *)
  let kwd_tbl =
    ["if", (IF, false);
     "else", (ELSE, false);
     "false", (FALSE, true);
     "for", (FOR, false);
     "func", (FUNC, false);
     "import", (IMPORT, false);
     "nil", (NIL, true);
     "package", (PACKAGE, false);
     "return", (RETURN, true);
     "struct", (STRUCT, false);
     "true", (TRUE, true);
     "type", (TYPE, false);
     "var", (VAR, false);
     "main", (MAIN, true);
     "fmt", (FMT, false);
    ]
    
  let semic = ref false

  let firstmain = ref true

  let id_or_kwd =
    let h = Hashtbl.create 17 in
    List.iter (fun (s,t) -> Hashtbl.add h s t) kwd_tbl;
    fun s ->
      try let m, b = List.assoc s kwd_tbl in (if m = MAIN 
      then (if !firstmain then (semic :=b ; firstmain := false; m)  else (semic := true ; IDENT s))
      else (semic := b ; m)) with _ -> (semic := true ; IDENT s)
      
  

}

let letter = ['a'-'z' 'A'-'Z']
let alpha = ['a'-'z' 'A'-'Z' '_']
let digit = ['0'-'9']
let ident = alpha (alpha | digit)*
let hexa = ['0'-'9' 'a'-'f' 'A'-'F']
let integer = ['0'-'9']+ | (("0x" | "0X") (hexa +))
let car =  [' ' '!' '#' - '[' ']' - '~'] | "\\\\" | "\\\"" | "\\n" | "\\t"
let chaine = ['"'] (car *) ['"']
let space = [' ' '\t']

rule token = parse
  | "//" [^ '\n']* '\n'  { new_line lexbuf; if !semic then (semic := false ; SEMICOLON) else token lexbuf }
  | '\n'    { new_line lexbuf; if !semic then (semic := false ; SEMICOLON) else token lexbuf }
  | space+  { token lexbuf }
  | ident as id { id_or_kwd id }
  | "++"    { semic := true ; PLUSP }
  | "--"    { semic := true ; MINUSM }
  | '+'     { semic := false ; PLUS }
  | '-'     { semic := false ; MINUS }
  | '*'     { semic := false ;TIMES }
  | '|'    { semic := false ; OR }
  | "&&"   { semic := false ; AAND }
  | '&'    { semic := false ; AND }
  | "!="    { semic := false ; NEQ }
  | '<'     { semic := false ; LESS }
  | "<="    { semic := false ; LEQ }
  | '>'     { semic := false ; GREATER }
  | ">="    { semic := false ; GEQ }   
  | "=="    { semic := false ; EEQUAL }
  | '='    { semic := false ; EQUAL }
  | '/'     { semic := false ; DIV }
  | '%'     { semic := false ; MOD }
  | '('     { semic := false ; LPAREN }
  | ')'     { semic := true ; RPAREN }
  | '{'     { semic := false ; BEGIN }
  | '}'     { semic := true ; END }
  | ','     { semic := false ; COMMA }
  | '.'     { semic := false ; DOT }
  | ';'     { semic := false ; SEMICOLON }
  | '!'     { semic := false ; EXCL }
  | ':'     { semic := false ; COLON}
  | "/*"    { comment lexbuf }
  | integer as s { semic := true ; try CST ( Nativeint.of_string ("-"^s)) with Failure(_)-> raise (Lexing_error ("number out of range"))}
  | ['"'] "fmt" ['"'] {semic := true ; FMT}
  | chaine as c { semic := true ; STR (c) }
  | "//" [^ '\n']* eof
  | eof     { EOF }
  | _ as c  { raise (Lexing_error ("illegal character: " ^ String.make 1 c)) }


and comment = parse
  | "*/"    { token lexbuf }
  | _       { comment lexbuf }
  | eof     { raise (Lexing_error ("unterminated comment")) }