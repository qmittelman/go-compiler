
(* Syntaxe abstraite pour Petit Go

*)

type posit = Lexing.position

type 'a pos = 'a * posit

type ident = string

type typ = 
  | TIdent of ident
  | TStar of typ

type type_retour = TR of typ list

type vars = Var of (ident list) * typ



type binop = Add | Sub | Mul | Div | Mod | And | Or | Eq | NEq | L | LEq | G | GEq

type unop = Plus | Minus | Star | UAnd | ExclaP

type expr =
  | Econst of nativeint
  | Estring of string
  | Ebool of bool
  | Enil
  | EPar of expr pos
  | Evar of ident
  | Edot of expr pos * ident
  | Eapp of ident * (expr pos list)
  | Eprint of (expr pos list)
  | Eunop of unop * expr pos
  | Ebinop of binop * expr pos * expr pos

type instr_simple =
  | Iexpr of expr pos
  | IPlus of expr pos
  | IMinus of expr pos
  | IEq of expr pos list * expr pos list
  | IAs of ident list * expr pos list

type instr = 
  | IS of instr_simple pos
  | IBloc of bloc pos
  | Instif of instr_if pos
  | IVar of ident list * typ pos list * expr pos list * posit
  | IReturn of expr pos list
  | Ifor of expr pos * bloc pos

and bloc = Bloc of instr list

and instr_if =
  | IIf of expr pos * bloc pos
  | Iifelse of expr pos * bloc pos * bloc pos
  | Iifelif of expr pos * bloc pos * instr_if pos



type strct = Struct of (ident * (vars pos list))


type functn = Funcr of  ident * (vars pos list) * type_retour pos list * bloc pos

type decl =
  | DecS of strct pos
  | Declf of functn pos

type fichier = {
  import_fmt : bool ;
  decls : decl list;
}


