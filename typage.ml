open Ast

type typego =
  | TGInt
  | TGBool
  | TGString
  | Stru of ident
  | Sta of typego
  | TGNil
  | TL of typego list

module IMap = Map.Make(struct type t = ident let compare = compare end)
module ISet = Set.Make(struct type t = ident let compare = compare end)

type funtype = posit * typego IMap.t * (typego) * ((ident * typego) list)
type structype = typego IMap.t

type struct_env = structype IMap.t
type function_env = funtype IMap.t

type var_env = (typego * bool ref * bool * posit) IMap.t (* Premier booleen : isUsed.
Second : isArgument. *)

type varnames = ISet.t

type env =
  {structs : struct_env ;
   functions : function_env ;
   vars : var_env}

let empty_env =
  {structs = IMap.empty ;
  functions = IMap.empty ;
  vars = IMap.empty}

type erreur =
  | NomNonUnique of (posit*ident)
  | TypeMalForme of posit
  | VarNonDef of posit
  | IntExpected of posit
  | BoolExpected of posit
  | TypeMismatch of posit
  | UnexpectedNil of posit
  | UnderscoreUtilise of posit
  | TypageGaucheAttendu of posit
  | StarExpected of posit
  | ChampNonReconnu of posit
  | NotAStructure of posit
  | FonctionNonDef of (posit* ident)
  | InvalidArgument of posit
  | BlocExpected of posit
  | NotEnoughToPrint of posit
  | UnusedEpression of posit
  | UnbalancedEq of posit
  | ExpectedSomething of posit
  | AssigningNil of posit
  | BadReturn of posit
  | UnusedVar of posit
  | NoMain of posit
  | BadMain of posit
  | FmtNonImporte of posit
  | FmtNonUtilise of posit
  | UnexpectedPrint of posit
  | NothingtoPrint of posit
  | ExpectedBool of posit
  | ExpectedReturn of posit
  | TypingError of posit

exception Typing_error of string * posit

let raise_err err =
  match err with
  | NomNonUnique(pos, id) -> raise (Typing_error("NomNonUnique: "^id, pos))
  | TypeMalForme(pos) -> raise (Typing_error("TypeMalForme", pos))
  | VarNonDef(pos) -> raise (Typing_error("VarNonDef", pos))
  | IntExpected(pos) -> raise (Typing_error("IntExpected", pos))
  | BoolExpected (pos)-> raise (Typing_error("BoolExpected", pos))
  | TypeMismatch (pos)-> raise (Typing_error("TypeMismatch", pos))
  | UnexpectedNil(pos) -> raise (Typing_error("UnexpectedNil", pos))
  | UnderscoreUtilise (pos)-> raise (Typing_error("UnderscoreUtilise", pos))
  | TypageGaucheAttendu(pos) -> raise (Typing_error("TypageGaucheAttendu", pos))
  | StarExpected (pos)-> raise (Typing_error("StarExpected", pos))
  | ChampNonReconnu(pos) -> raise (Typing_error("ChampNonReconnu", pos))
  | NotAStructure (pos)-> raise (Typing_error("NotAStructure", pos))
  | FonctionNonDef(pos, id) -> raise (Typing_error("FonctionNonDef: "^id, pos))
  | InvalidArgument(pos) -> raise (Typing_error("InvalidArgument", pos))
  | BlocExpected (pos)-> raise (Typing_error("BlocExpected", pos))
  | NotEnoughToPrint(pos) -> raise (Typing_error("NotEnoughToPrint", pos))
  | UnusedEpression(pos) -> raise (Typing_error("UnusedEpression", pos))
  | UnbalancedEq (pos)-> raise (Typing_error("UnbalancedEq", pos))
  | ExpectedSomething(pos) -> raise (Typing_error("ExpectedSomething", pos))
  | AssigningNil (pos)-> raise (Typing_error("AssigningNil", pos))
  | BadReturn(pos) -> raise (Typing_error("BadReturn", pos))
  | UnusedVar(pos) -> raise (Typing_error("UnusedVar", pos))
  | NoMain(pos) -> raise (Typing_error("NoMain", pos))
  | BadMain(pos) -> raise (Typing_error("BadMain", pos))
  | FmtNonImporte(pos) -> raise (Typing_error("FmtNonImporte", pos))
  | FmtNonUtilise(pos) -> raise (Typing_error("FmtNonUtilise", pos))
  | UnexpectedPrint(pos) -> raise (Typing_error("UnexpectedPrint", pos))
  | NothingtoPrint(pos) -> raise (Typing_error("NothingtoPrint", pos))
  | ExpectedBool(pos) -> raise (Typing_error("ExpectedBool", pos))
  | ExpectedReturn(pos) ->raise (Typing_error("ExpectedReturn", pos))
  | TypingError(pos) -> raise (Typing_error("TypingError", pos))

  let dummy_p = Lexing.dummy_pos

let first = function
|(a,b,c) -> a

let give_l_t = function
  |TL(l) -> l
  |_ -> assert false

let rec typ_to_typego = function
  | TIdent(s) -> if s = "int" then TGInt else if s="bool" then TGBool else if s = "string" then TGString else Stru(s)
  | TStar(t) -> Sta(typ_to_typego t)

let rec typego_to_typ pos = function
| TGInt -> TIdent("int"), pos
| TGBool ->TIdent("bool"), pos
| TGString -> TIdent("string"), pos
| Stru(ident) -> TIdent(ident), pos
| Sta(t) -> TStar(fst (typego_to_typ pos t)), pos
| TGNil -> raise_err (AssigningNil(pos))
| TL(t::q) -> raise_err (UnbalancedEq(pos))

let rec bien_forme env = function  
  | Sta(t) -> bien_forme env t
  | Stru(id) -> IMap.mem id env.structs
  | _ -> true

let rec equal_type env pos t1 t2 = 
  match t1, t2 with
  | Sta(t3) , TGNil -> if bien_forme env t3 then true else raise_err ((TypeMalForme(pos)))
  | TGNil , Sta(t3) -> equal_type env pos t2 t1
  | Sta(t3) , Sta(t4) -> equal_type env pos t3 t4
  | _ -> t1 = t2

let rec equal_type_list env pos l1 l2 =
  match l1, l2 with
  | TGNil , TGNil -> true
  | TL(l1), TL(l2) ->begin match l1, l2 with
  | [] , [] -> true
  | t1::q1 , [] -> false
  | [], t1::q1 -> false
  | t1::q1, t2::q2 -> if equal_type env pos t1 t2 then equal_type_list env pos (TL(q1)) (TL(q2)) else false
  end
  | _  -> false


let type_retour_bf env pos = function
  | [] -> TGNil
  | (t, pos)::q -> (let res = ref [] in 
              match t with
             | TR(l) -> ((List.iter (fun t -> let tgo = (typ_to_typego t) in if bien_forme env tgo then res := tgo::!res else raise_err ((TypeMalForme(pos)))) l); TL(List.rev !res))
             | _ -> assert false )


let add_struct env = function
| Declf _ -> env
| DecS((Struct(s, vars), pos)) ->
  if IMap.mem s env.structs then
    raise_err ((NomNonUnique(pos, s)))
  else
    {structs = IMap.add s IMap.empty env.structs ;
     functions = env.functions ;
     vars = env.vars}
| DecS(_)-> assert false


let add_fun env = function
| DecS(_) -> env
| Declf((Funcr(fid, vars, l_type_retour, _), pos)) ->
if IMap.mem fid env.functions then
raise_err ((NomNonUnique(pos,fid)))
else (
  let arguments = ref IMap.empty in
  let arguments_list = ref [] in
  List.iter (fun (Var(idl, t), pos2) -> let tbf = typ_to_typego t in if bien_forme env tbf then (List.iter (fun id -> if IMap.mem id !arguments then raise_err ((NomNonUnique(pos,id)))
                                                                                             else (arguments_list := (id, tbf)::!arguments_list ; arguments := IMap.add id tbf !arguments)) idl )
                                                          else raise_err ((TypeMalForme pos2))) vars ;
  let l_typego_retour = type_retour_bf env pos l_type_retour in
  {structs = env.structs ;
     functions = IMap.add fid (pos, !arguments, l_typego_retour, List.rev !arguments_list) env.functions ;
     vars = env.vars}
)
|_ -> assert false

let add_struct_field env = function
| Declf _ -> env
| DecS((Struct(fid, vars)), pos) ->
(
  let arguments = ref IMap.empty in
  List.iter (fun (Var(idl, t), pos2) -> if bien_forme env (typ_to_typego t) then (List.iter (fun id -> if IMap.mem id !arguments then raise_err ((NomNonUnique (pos2,id)))
                                                            else arguments := IMap.add id (typ_to_typego t) !arguments) idl )
                            else raise_err ((TypeMalForme pos2) )) vars ;
  {structs = IMap.add fid !arguments env.structs ;
     functions = env.functions ;
     vars = env.vars}
)
| DecS(_)-> assert false

let rec type_expr env = function  (* Prends en argument env expr et renvoie (type_expr expr est_type_gauche)*)
  | Econst(n), pos -> (TGInt, (Econst(n),pos), false)
  | Estring(s), pos -> (TGString, (Estring(s),pos), false) 
  | Ebool(b),pos -> (TGBool, (Ebool(b),pos), false)
  | Enil, pos -> (TGNil, (Enil,pos), false)
  | EPar(e) ,pos -> type_expr env e
  | Evar(id), pos when id <> "_" -> (try let  (t, isUsed, isArg, pos) = IMap.find id env.vars in isUsed := true ; (t, (Evar(id),pos), true)
                with
                | Not_found -> raise_err ((VarNonDef pos)))
  | Evar(_), pos -> raise_err ((UnderscoreUtilise pos))
  | Ebinop(binop,e1, e2), pos -> (match binop with
                            | Add | Sub | Mul | Div | Mod -> (let (t1, exp, bool) = type_expr env e1 in 
                            if t1 <> TGInt then raise_err ((IntExpected pos))
                            else let t2, _, _ = type_expr env e2 in 
                            if t2 <> TGInt then raise_err ((IntExpected pos))
                            else (TGInt, (Ebinop(binop,e1, e2),pos), false))
                            | L | LEq | G | GEq -> (let t1, _,_ = type_expr env e1 in 
                            if t1 <> TGInt then raise_err ((IntExpected pos))
                            else let t2, _, _ = type_expr env e2 in 
                            if t2 <> TGInt then raise_err ((IntExpected pos))
                            else (TGBool, (Ebinop(binop,e1, e2),pos), false))
                            | Eq | NEq -> (let t1, _, _ = type_expr env e1 in 
                            let t2, _,_ = type_expr env e2 in 
                            if t1 <> t2 then raise_err ((TypeMismatch pos))
                            else if (fst e1 <> Enil || fst e2 <> Enil) then (TGBool, (Ebinop(binop,e1, e2),pos), false) else raise_err ((UnexpectedNil pos))))
  | Eunop(uno,exp),pos  -> begin match uno with
                      | Minus -> let t,_,_ = (type_expr env exp) in if t <> TGInt then raise_err ((IntExpected pos)) else (TGInt, (Eunop(uno,exp),pos), false)
                      | UAnd -> let t,_, b = (type_expr env exp) in if b then (Sta(t), (Eunop(uno,exp),pos), false) else raise_err ((TypageGaucheAttendu pos))
                      | Star -> begin
                                 match type_expr env exp with
                                 | Sta(t), e, _ when fst e <> Enil ->  (t,(Eunop(uno,exp),pos), false)
                                 | Sta(t), e, _ -> raise_err ((UnexpectedNil pos))
                                 | _ -> raise_err ((StarExpected pos))
                      end
                      | ExclaP -> let t,_,_ = (type_expr env exp) in if t <> TGBool then raise_err ((IntExpected pos)) else (TGBool, (Eunop(uno,exp),pos), false)
                    end
| Edot(e,id), pos -> begin 
                  match type_expr env e with
                  | (Stru(s), expr, isgauche) ->
                                begin 
                                  try let t = IMap.find id (IMap.find s env.structs) in
                                   (t, (Edot(e, id),pos), isgauche)
                                  with
                                  | Not_found -> raise_err ((ChampNonReconnu pos))
                                end
                  | (Sta(Stru s), expr, isgauche) ->
                                begin 
                                  try let t = IMap.find id (IMap.find s env.structs) in
                                  (t, ((Edot (e, id),pos)), isgauche)
                                  with
                                  | Not_found -> raise_err ((ChampNonReconnu pos)) 
                                end
                  |  _ -> raise_err ((NotAStructure pos))
                  end
  | Eapp(id, exp_l), pos -> type_app env (Eapp(id, exp_l),pos)
  | Eprint(_), pos -> raise_err ((UnexpectedPrint pos))


and type_app env = function  (* Prends en argument env Eapp(id, exp_l) et renvoie (type_expr, Eapp(id, exp_l), est_type_gauche)*)
 |Eapp(id, exp_l), pos -> begin
                        try 
                          begin 
                          let (posapp, args_Map, typ_ret, args_list) = IMap.find id env.functions in
                          let typ_attendu_args = TL(snd (List.split args_list)) in
                          let types_donnes_args = (List.map (fun x ->  first (type_expr env x)) exp_l) in
                          match exp_l with
                          | [(Eapp(id_g, exp_l_g), pos1)] -> let type_g = List.hd types_donnes_args in if equal_type_list env pos typ_attendu_args type_g then (typ_ret, (Eapp(id, exp_l),pos), false) else  raise_err ((InvalidArgument pos))
                          | _ -> if equal_type_list env pos typ_attendu_args (TL(types_donnes_args)) then (typ_ret, (Eapp(id, exp_l),pos), false) else  raise_err ((InvalidArgument pos))
                          end
                        with Not_found -> raise_err ((FonctionNonDef(pos, id)))
                      end
  | _ -> assert false

let rec str_doublons pos= function
 |[] -> ()
 | t::q -> if List.mem t q then raise_err (((NomNonUnique (pos, t)))) else str_doublons pos q

let var_doublons env pos l_id =
 List.iter (fun x -> if IMap.mem x env.vars then let t, isUsed, isArg, pos1 = IMap.find x env.vars in raise_err ((NomNonUnique (pos,x)))) l_id

let type_fun env ident  posf vars_l type_retour_l bloc =
  let rec type_bloc env type_retour_l = function
  (* Renvoie (AUnRetourDansChaqueBranche, UtilisePrint, bloc) si le bloc est bien typé *)
  | Bloc(l) ->
      begin
         match l with
         | [] -> IMap.iter (fun key (t, isUsed, isArg, pos) -> if ((key = "_")||(!isUsed || isArg)) then () else raise_err (UnusedVar(pos))) env.vars ; false, false, []
         | IS(instr_s, pos)::q -> begin
                                match instr_s with
                                |Iexpr(Eprint([(Eapp(id, exp_l), pos1)]), pos2) -> begin
                                                                    let type_ret = first (type_app env ((Eapp(id, exp_l)),pos1)) in
                                                                    match type_ret with
                                                                    | TL(t2::q2) when List.length (t2::q2) > 0 -> 
                                                                    let aDesRetours, usePrint, resteBloc = type_bloc env type_retour_l (Bloc(q)) 
                                                                    in (aDesRetours, true, IS(Iexpr(Eprint([Eapp(id,exp_l), pos1]), pos2),pos)::resteBloc)
                                                                    | _ -> raise_err ((NotEnoughToPrint pos2))
                                                                    end
                                |Iexpr(Eprint(exp_l),pos1) -> if exp_l = [] then raise_err ((NothingtoPrint pos1)) 
                                                         else let exp_l_typee = (List.map (type_expr env) exp_l) in 
                                                         let aDesRetours, usePrint, resteBloc = type_bloc env type_retour_l (Bloc(q)) 
                                                         in (aDesRetours, true, IS(Iexpr(Eprint(exp_l), pos1), pos)::resteBloc)
                                |Iexpr(e, pos1) -> let tep = type_expr env (e, pos1) in 
                                                   let aDesRetours, usePrint, resteBloc = type_bloc env type_retour_l (Bloc(q)) 
                                                  in (aDesRetours, usePrint, IS(Iexpr(e, pos1), pos)::resteBloc)
                                |IPlus(e) -> let aDesRetours, usePrint, resteBloc =type_bloc env type_retour_l (Bloc(q)) 
                                              in let t_e, exp, est_g = type_expr env e in
                                              if est_g then (if t_e = TGInt then (aDesRetours, usePrint, (IS(IPlus(e),pos))::resteBloc) else raise_err ((IntExpected pos)))
                                              else raise_err ( (TypageGaucheAttendu pos))
                                |IMinus(e) -> let aDesRetours, usePrint, resteBloc =type_bloc env type_retour_l (Bloc(q)) 
                                              in let t_e, exp, est_g = type_expr env e in
                                              if est_g then (if t_e = TGInt then (aDesRetours, usePrint, IS(IMinus(e),pos)::resteBloc) else raise_err ((IntExpected pos)))
                                              else raise_err ( (TypageGaucheAttendu pos))
                                | IEq(expr_l, [(Eapp(id, exp_l2),pos1)]) -> let n1 = List.length expr_l in
                                                                    let expr_l_t = List.map (fun x -> let tx, ex, bx = (type_expr env x) in if bx then tx else raise_err ((TypageGaucheAttendu pos1))) expr_l in 
                                                                   let type_ret, (exp, p), _ = (type_app env (Eapp(id, exp_l2), pos1)) in
                                                                   begin
                                                                   match type_ret with
                                                                   | TL(t2::q2) when List.length (t2::q2) = n1 -> 
                                                                   if equal_type_list env p (TL(expr_l_t)) type_ret then
                                                                   let aDesRetours, usePrint, resteBloc =type_bloc env type_retour_l (Bloc(q)) 
                                                                   in (aDesRetours, usePrint,  IS(IEq(expr_l, [Eapp(id, exp_l2), pos1]),pos)::resteBloc)
                                                                   else raise_err ((TypeMismatch pos))
                                                                   | _ -> raise_err ((UnbalancedEq pos))
                                                                   end
                                | IEq(expr_l, expr_l2) when (List.length expr_l) = (List.length expr_l2) ->let t1 = List.map (fun x -> let tx, ex, bx = (type_expr env x) in if bx then tx else raise_err ((TypageGaucheAttendu pos))) expr_l in
                                                   let t2 = List.map (fun x -> first (type_expr env x)) expr_l2 in if equal_type_list env pos (TL(t1)) (TL(t2)) then
                                                   let aDesRetours, usePrint, resteBloc =type_bloc env type_retour_l (Bloc(q)) 
                                                   in (aDesRetours, usePrint, IS(IEq(expr_l, expr_l2), pos)::resteBloc)
                                                   else raise_err ((TypeMismatch pos))
                                | IAs(expr_l, [(Eapp(id, exp_l2),pos1)]) ->  let type_ret = first (type_app env (Eapp(id, exp_l2),pos1)) in
                                                                   begin
                                                                   match type_ret with
                                                                   | TL(t2::q2) when List.length (t2::q2) = List.length expr_l -> let ltype_ret = t2::q2 in
                                                                   let v_a_decl = ref [] in
                                                                   List.iter2 (fun e tg -> let t = typego_to_typ pos1 tg in if IMap.mem id env.vars then () else v_a_decl := (IVar([id], [t], [], pos)::!v_a_decl)) expr_l ltype_ret ;
                                                                   if !v_a_decl <> [] then type_bloc env type_retour_l (Bloc(List.append !v_a_decl ((IS(IAs(expr_l, [(Eapp(id, exp_l2),pos1)]),pos))::q)))
                                                                   else
                                                                   let t1 = List.map (fun x -> try let t_v, isUsed, isArg, posv = (IMap.find x env.vars) in (t_v) 
                                                                        with |Not_found -> raise_err ((VarNonDef pos))) expr_l
                                                                   in
                                                                   if equal_type_list env pos (TL(t1)) type_ret then
                                                                   let aDesRetours, usePrint, resteBloc =type_bloc env type_retour_l (Bloc(q)) 
                                                                   in (aDesRetours, usePrint,  IS(IAs(expr_l, [Eapp(id, exp_l2), pos1]), pos)::resteBloc)
                                                                   else raise_err ((TypeMismatch pos))
                                                                   | _ -> raise_err ((UnbalancedEq pos))
                                                                   end
                                | IAs(id_l, expr_l) -> if List.length id_l = List.length expr_l then (let v_a_decl = ref [] in
                                  List.iter2 (fun id e -> if IMap.mem id env.vars then () else v_a_decl := (IVar([id], [], [e], pos)::!v_a_decl)) id_l expr_l ;
                                  if !v_a_decl <> [] then type_bloc env type_retour_l (Bloc(List.append !v_a_decl ((IS(IAs(id_l, expr_l), pos))::q)))
                                  else let t1 = List.map (fun x -> try let t_v, isUsed, isArg, posv = (IMap.find x env.vars) in  t_v 
                                     with |Not_found -> assert false) id_l
                                                      in let t2 = List.map (fun x -> first (type_expr env x)) expr_l in if equal_type_list env pos (TL(t1)) (TL(t2)) then
                                                      let aDesRetours, usePrint, resteBloc =type_bloc env type_retour_l (Bloc(q)) 
                                                       in (aDesRetours, usePrint, IS(IAs(id_l, expr_l), pos)::resteBloc) else raise_err ((UnbalancedEq pos)) )
                                                     else raise_err ((UnbalancedEq pos))                                                           
                              end
          | IBloc(block2, pos)::q -> let aDesRetours, usePrint, resteBloc =type_bloc env type_retour_l (Bloc(q))
                               in let vars_hors_bloc2 = IMap.map (fun (t, isUsed, isArgument, posv) -> (t, isUsed, true, posv)) env.vars in
                               let new_env = {structs = env.structs ;
                               functions = env.functions ;
                               vars = vars_hors_bloc2} in
                               let aDesRetours2, usePrint2, tBloc2 =type_bloc new_env type_retour_l  block2 in
                               (aDesRetours, usePrint || usePrint2, IBloc(Bloc(tBloc2), pos)::resteBloc)
          | Instif((IIf(expr, bl), pos))::q -> let aDesRetours, usePrint, resteBloc =type_bloc env type_retour_l (Bloc(q))
                                        in let aDesRetours2, usePrint2, resteBloc2 =type_bloc env type_retour_l (fst bl) 
                                        in if first (type_expr env expr) = TGBool then
                                        (aDesRetours, usePrint || usePrint2, Instif(IIf(expr, bl),pos)::resteBloc)
                                        else raise_err ((ExpectedBool (snd expr)))
          | Instif((Iifelse(expr,b1, b2)),pos)::q -> let aDesRetours, usePrint, resteBloc =type_bloc env type_retour_l (Bloc(q))
                                              in let aDesRetours2, usePrint2, resteBloc2 =type_bloc env type_retour_l (fst b1) 
                                              in let aDesRetours3, usePrint3, resteBloc3 =type_bloc env type_retour_l (fst b2) 
                                              in if first (type_expr env expr) = TGBool then
                                              ((aDesRetours2 && aDesRetours3) || aDesRetours, (usePrint || usePrint2) || usePrint3 , Instif(Iifelse(expr,(Bloc(resteBloc2),snd b1), (Bloc(resteBloc3), snd b2)), pos)::resteBloc)
                                              else raise_err ((ExpectedBool (snd expr)))
          | Instif((Iifelif(expr,bloc,instr_if)),pos)::q -> type_bloc env type_retour_l (Bloc(Instif(Iifelif(expr,bloc, instr_if),pos)::q))
          | IVar(id_l, typ_l, expr_l, pos)::q -> 
                                              begin
                                              str_doublons pos id_l ;
                                              var_doublons env pos id_l ;
                                              match typ_l with
                                              | [typ_im, p] -> begin let typ_imp = typ_to_typego typ_im in          
                                                             match expr_l with
                                                             | [] -> if bien_forme env typ_imp then (let new_var = ref env.vars in
                                                             (List.iter (fun x -> let b = ref false in new_var := (IMap.add x (typ_imp, b, false, p) !new_var)) id_l ;
                                                             let env2 = {structs = env.structs ;
                                                             functions = env.functions ;
                                                             vars = !new_var} in let aDesRetours, usePrint, resteBloc =type_bloc env2 type_retour_l (Bloc(q))
                                                             in (aDesRetours, usePrint, (IVar(id_l, typ_l, expr_l, pos))::resteBloc))) else raise_err ((TypeMalForme pos))
                                                             | [Eapp(id, exp_l2), pos1] -> let type_ret = give_l_t (first (type_app env (Eapp(id, exp_l2), pos1))) in
                                                                                      if List.length type_ret <> List.length id_l then raise_err ((UnbalancedEq pos1))
                                                                                      else try let k = List.find (fun t -> t <> typ_imp) type_ret in raise_err ((TypeMismatch pos))
                                                                                      with Not_found -> if bien_forme env typ_imp then (let new_var = ref env.vars in
                                                                                      (List.iter (fun x -> let b = ref false in new_var := (IMap.add x (typ_imp, b, false, pos1) !new_var)) id_l ;
                                                                                      let env2 = {structs = env.structs ;
                                                                                      functions = env.functions ;
                                                                                      vars = !new_var} in let aDesRetours, usePrint, resteBloc =type_bloc env2 type_retour_l (Bloc(q))
                                                                                      in (aDesRetours, usePrint, IVar(id_l, typ_l, expr_l, pos)::resteBloc))) else raise_err ((TypeMalForme pos))
                                                             | _ -> let t2 = List.map (fun x -> first (type_expr env x)) expr_l in
                                                             if List.length t2 <> List.length id_l then raise_err ((UnbalancedEq pos))
                                                             else try let k = List.find (fun t -> t <> typ_imp) t2 in raise_err ((TypeMismatch pos))
                                                             with Not_found -> if bien_forme env typ_imp then (let new_var = ref env.vars in
                                                             (List.iter (fun x -> let b = ref false in new_var := (IMap.add x (typ_imp, b, false, p) !new_var)) id_l ;
                                                             let env2 = {structs = env.structs ;
                                                             functions = env.functions ;
                                                             vars = !new_var} in let aDesRetours, usePrint, resteBloc =type_bloc env2 type_retour_l (Bloc(q))
                                                             in (aDesRetours, usePrint, IVar(id_l, typ_l, expr_l, pos)::resteBloc))) else raise_err ((TypeMalForme pos))
                                                             end
                                              | [] -> begin
                                                     match expr_l with
                                                     | [] -> raise_err ((ExpectedSomething pos))
                                                     | [Eapp(id, exp_l2), pos2] -> let type_ret = give_l_t (first (type_app env (Eapp(id, exp_l2), pos2))) in
                                                                        if List.length type_ret <> List.length id_l then raise_err ((UnbalancedEq pos))
                                                                         else (let new_var = ref env.vars in
                                                                      (List.iter2 (fun i ti -> let b = ref false in new_var := (IMap.add i (ti, b, false, pos2) !new_var)) id_l type_ret;
                                                                         let env2 = {structs = env.structs ;
                                                                                     functions = env.functions ;
                                                                                      vars = !new_var} in let aDesRetours, usePrint, resteBloc =type_bloc env2 type_retour_l (Bloc(q))
                                                                               in (aDesRetours, usePrint, IVar(id_l, typ_l, expr_l, pos)::resteBloc))) 
                                                     | _ -> if List.exists (fun (t,p) -> (t=Enil)) expr_l then raise_err ( (AssigningNil pos)) else let t2 = List.map (fun x -> first (type_expr env x)) expr_l in
                                                                    if List.length t2 <> List.length id_l then raise_err ((UnbalancedEq pos))
                                                                 else  (let new_var = ref env.vars in
                                                                 (List.iter2 (fun i ti -> let b = ref false in new_var := (IMap.add i (ti, b, false, pos) !new_var)) id_l t2;
                                                                let env2 = {structs = env.structs ;
                                                                 functions = env.functions ;
                                                                 vars = !new_var} in let aDesRetours, usePrint, resteBloc =type_bloc env2 type_retour_l (Bloc(q))
                                                                 in (aDesRetours, usePrint, IVar(id_l, typ_l, expr_l, pos)::resteBloc)))
                                                    end
                                              | _ -> assert false
                                         end
          | IReturn((et,pos)::eq)::q -> let expr_l = (et,pos)::eq in let type_return_donnes = List.map (fun x -> first (type_expr env x)) expr_l in
                                  let aDesRetours, usePrint, resteBloc =type_bloc env type_retour_l (Bloc(q)) in
                                  if equal_type_list env pos (TL(type_return_donnes)) type_retour_l then
                                  (true, usePrint, IReturn(expr_l)::resteBloc)
                                  else raise_err ((BadReturn pos))
          | Ifor((expr,pos), blc)::q -> let boolExpected = first (type_expr env (expr,pos)) in if boolExpected <> TGBool then raise_err ((BoolExpected pos))
                                else let aDesRetours, usePrint, resteBloc = type_bloc env type_retour_l (fst blc) in
                                let aDesRetours2, usePrint2, resteBloc2 =type_bloc env type_retour_l (Bloc(q)) in
                                (aDesRetours, usePrint || usePrint2, Ifor((expr,pos), (Bloc(resteBloc),(snd blc)))::resteBloc2)
          | _ -> raise_err (TypingError(dummy_p))
      end
  | _ -> assert false
  in let pos_f , args_m, type_ret, args_l = IMap.find ident env.functions in
  let ajoute_arg map_arg (id, tg) = IMap.add id (tg, ref false, true, posf) map_arg in
  let varis = List.fold_left ajoute_arg IMap.empty args_l in
  let env = {env with vars = varis} in
  let aDesRetours, usePrint, bl = type_bloc env type_ret bloc in
  if (not(aDesRetours) && (type_ret <> TGNil)) then raise_err ((ExpectedReturn posf))
  else (usePrint, bl)

let check_struct_rec structs= ()

let check_main env =
  try 
     let pos_main, _, type_ret, arg_l = IMap.find "main" env.functions
     in match type_ret, arg_l with
     | TL([]) , []
     | TL([TGNil]) , []
     | TGNil, [] -> ()
     | _ -> raise_err ((BadMain pos_main))
  with 
  |Not_found -> raise_err ((NoMain dummy_p))

let typage p =
  let fmtImporte = p.import_fmt in
  let decl_l = p.decls in
  let env = List.fold_left add_struct empty_env decl_l in
  let env = List.fold_left add_fun env decl_l in
  let env = List.fold_left add_struct_field env decl_l in
  check_struct_rec env.structs ;
  let rec read_function is_print = function
  | [] -> is_print, []
  | (DecS _) :: l -> read_function is_print l
  | (Declf (Funcr(ident,vars_l,type_retour_l, bloc), posf)) :: l -> let usePrint, new_f = type_fun env ident posf vars_l type_retour_l (fst bloc) in
    let (usePrint2, blocc) = read_function (is_print || usePrint) l in
    (usePrint2 || usePrint, (new_f) :: blocc) in
let (is_print, functions) = read_function false decl_l in
check_main env ;
match is_print, fmtImporte with
| true, false -> raise_err ((FmtNonImporte dummy_p)) 
| false, true -> raise_err ((FmtNonUtilise dummy_p)) 
| _, _ -> env, functions